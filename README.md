# Next.js Comments Widget SaaS

## Features

- **Customizable Comments Widget:** Easily integrate a comments section into your website, with extensive customization options to match your site's design.
- **Real-time Database:** Utilizing Firebase/Firestore for real-time data storage and retrieval, ensuring that comments are displayed and updated in real-time.
- **Secure & Scalable:** 
- **Subscription Payment System:** Includes a built-in subscription model, enabling monetization of the service through recurring payments.
- **Easy Deployment:** Leveraging Vercel for deployment processes

## Getting Started

### Prerequisites

- Node.js (latest version)
- Firebase account
- Vercel account (for deployment)
- Stripe account (for managing subscriptions)

### Setup

1. **Clone the repository**


2. **Install dependencies**

```bash
npm install
```

3. **Configure your environment**

Rename `.env.local.example` to `.env.local` and update it with your Firebase, Vercel, and Stripe keys.

4. **Run the development server**

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) to see the application in action.

5. **Deploy on Vercel**

Follow the [Vercel deployment documentation](https://vercel.com/docs) to get your application live.

